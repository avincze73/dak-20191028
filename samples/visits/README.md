# Building the visits application

At first  start the application in an imperative way.



```bash
docker build -t avincze73/visits:v1 .

# The following command results in an error
# because of a missing redis server
docker run avincze73/visits:v1

# This is not enough
docker run redis 
```

Using docker-compose
```bash
docker-compose up
docker-compose up --build

docker-compose up -d
docker-compose down
```

docker-compose with crash
```bash
docker-compose up --build
```

docker-compose ps
```bash
docker-compose ps 

```