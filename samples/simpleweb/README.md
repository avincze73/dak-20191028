Building the simpleweb application
```
docker build -t avincze73/simpleweb:v1 .
# This is executed inside a container within a network
# Accessing simpleweb application we need port forwarding
docker run -p 8080:8080 avincze73/simpleweb

# Check the filesystem of the container
docker run -it avincze73/simpleweb bash
docker exec -it <container id> bash
```