# How to install the complex application with docker compose?
From complex3 directory use the following commands.
```bash
docker system prune -a
docker volume prune
docker-compose up --build
docker build -t avincze73/multi-client -f ./client/Dockerfile.dev ./client
docker build -t avincze73/multi-worker -f ./client/Dockerfile.dev ./worker
docker build -t avincze73/multi-server -f ./client/Dockerfile.dev ./server
docker push avincze73/multi-client
docker push avincze73/multi-server
docker push avincze73/multi-worker
kubectl apply -f k8s/

kubectl create secret generic pgpassword --from-literal PGPASSWORD=titkos123

# Ingress setup on minikube
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
minikube addons enable ingress


```

# Integrating the complex project with CI

## Create the Dockerfiles for each project.
