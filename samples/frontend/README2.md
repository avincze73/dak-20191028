In the root project directory, create a file called .env

Add the following text to the file and save it: CHOKIDAR_USEPOLLING=true

That's all!



```bash

docker run -p 3000:3000 -v $(pwd):/app containerid

# first v is a bookmark to that folder
docker run -p 3000:3000 -v /app/node_modules -v $(pwd):/app containerid

```