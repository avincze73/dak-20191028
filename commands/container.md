# Containers

## Check Our Docker Install and Config

We have management commands
```bash
docker version

docker info

# Listing the commands and management commands
# docker command subcommand options 
docker

#This is the new way
docker container run

#This is the old way
docker run
```

Woring with nginx container
```bash
# A new unique id is created
# Explain the part of the commands
docker container run --publish 80:80 nginx

# Docker is in the background
docker container run --publish 80:80 --detach nginx

# It only shows running containers
docker container ls

docker container stop <container id>

docker container ls

# All containers are listed
docker container ls -a

docker container run --publish 80:80 --detach --name sprint-webhost nginx

docker container ls -a

#Checking the log events
docker container logs webhost

docker container top

# This is the process running in the container
# Lists runnning processes in the container
docker container top webhost

docker container --help

docker container ls -a

docker container rm 63f 690 ode

docker container ls

docker container rm -f 63f

docker container ls -a
```

## Containers as processes
Explain what is happening when the docker run command is executed.
```bash
docker run --name mongo -d mongo
docker top mongo
ps aux | grep mongod
docker stop mongo
ps aux | grep mongod
docker start mongo
docker top mongo
ps aux | grep mongod
```

## What's Going On In Containers: CLI Process Monitoring

```bash
docker container run -d --name nginx nginx

docker container run -d --name mysql -e MYSQL_RANDOM_ROOT_PASSWORD=true mysql

docker container ls

docker container top mysql

docker container top nginx

docker container inspect mysql

docker container stats --help

# It gives performance data
docker container stats

docker container ls
```


## Getting a Shell Inside Containers: No Need for SSH

```bash
docker container run -help
# If we exit the shell the container stops
docker container run -it --name proxy nginx bash

docker container ls

docker container ls -a

# the default shell is bash
docker container run -it --name ubuntu ubuntu
# Go inside and execute commands
apt-get update
curl google.com

docker container ls

docker container ls -a

docker container start --help

# attach and interactive mode
docker container start -ai ubuntu

docker container exec --help

docker container exec -it mysql bash

docker container ls

# It is very small sized
docker pull alpine


docker image ls

# It does not have bash
docker container run -it alpine bash

docker container run -it alpine sh
```

## Docker Networks: CLI Management of Virtual Networks

```bash
docker container run -p 80:80 --name webhost -d nginx

docker container port webhost

docker container inspect --format '{{ .NetworkSettings.IPAddress }}' webhost


docker network ls

docker network inspect bridge

docker network ls

docker network create my_app_net

docker network ls

docker network create --help

docker container run -d --name new_nginx --network my_app_net nginx

docker network inspect my_app_net

docker network --help

# we can connect a container to a new network
docker network connect

docker container inspect TAB COMPLETION

docker container disconnect TAB COMPLETION

docker container inspect
```


## Docker Networks: DNS and How Containers Find Each Other

```bash
docker container ls

docker network inspect TAB COMPLETION

docker container run -d --name my_nginx --network my_app_net nginx

docker container inspect TAB COMPLETION

#Install with apt-get install iputils-ping
docker container exec -it my_nginx ping new_nginx

docker container exec -it new_nginx ping my_nginx

docker network ls

docker container create --help
```


```bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -a -q)

docker container run -d --name nginx1 --network nginx_network nginx
docker container run -d --name nginx2 --network nginx_network nginx
```