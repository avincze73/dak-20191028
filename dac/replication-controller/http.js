const http = require('http')
const port = 8080

const requestHandler = (request, response) => {
  console.log(request.url);
  response.writeHead(200);
  response.end('Hello kubernetes from express!');
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err);
  }

  console.log(`server is listening on ${port}`);
})