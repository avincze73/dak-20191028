minikube start
eval $(minikube docker-env)
docker build --rm -t avincze73/k8s-hello-world:v1 .
docker push avincze73/k8s-hello-world:v1
kubectl apply -f helloworld-replication-controller.yaml
kubectl get pods
kubectl describe pod helloworld-replication-controller-4tnqn
kubectl delete pod helloworld-replication-controller-4tnqn
kubectl scale --replicas=5 -f hello-world-replication-controller.yaml
kubectl get rc
kubectl scale --replicas=2 rc/helloworld-replication-controller
kubectl scale --replicas=5 rc/helloworld-replication-controller
kubectl delete rc/helloworld-replication-controller