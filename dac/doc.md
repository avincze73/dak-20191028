# Microservices

---
## Concept


- Monolithic applications do not work with each other to complete their tasks, those live as separate islands.
- Microservice architecture is assembled by small services and those make up the applications.
- Microservice model is very hot-topic today, applications to towards it.
- The microservices are independent from each other.
- One large application wires many independent microservice.

![Monolithic](image/03-monolithic.png)

![Microservice](image/03-microservice.png)

---
## Service Mesh

Service mesh is a network of microservices that make up such applications and the interactions between them.

If microservices talk to each other there are no
- encryption
- failover
- loadbalancing
- routing dispatch
- access control

We can inject the above services with proxies and it can result the following architecture

![Proxies](image/03-proxies.png)

With microservice proxies we have
- encryption
- failover
- loadbalancing
- routing dispatch
- access control

---
# Istio


![Proxies](https://istio.io/docs/concepts/what-is-istio/arch.svg)

---
## What is a service mesh?

Service mesh is a network of microservices that make up such applications and the interactions between them. Service mesh can include discovery, load balancing, failure recovery, metrics, and monitoring.

---
## What is Istio?

It easy to create a network of deployed services with load balancing, service-to-service authentication, monitoring, and more, with few or no code changes in service code. Istio deploys a special sidecar proxy throughout your environment that intercepts all network communication between microservices.


An Istio service mesh is logically split into a data plane and a control plane.
- The **data plane** is composed of a set of intelligent proxies (Envoy) deployed as sidecars. These proxies mediate and control all network communication between microservices along with Mixer, a general-purpose policy and telemetry hub.
- The **control plane** manages and configures the proxies to route traffic. Additionally, the control plane configures Mixers to enforce policies and collect telemetry.


### Envoy proxy
Envoy is a high-performance proxy developed in C++ to mediate all inbound and outbound traffic for all services in the service mesh. 

- Dynamic service discovery
- Load balancing
- TLS termination
- HTTP/2 and gRPC proxies
- Circuit breakers
- Health checks
- Staged rollouts with %-based traffic split
- Fault injection
- Rich metrics

Envoy is deployed as a sidecar to the relevant service in the same Kubernetes pod. The sidecar proxy model also allows you to add Istio capabilities to an existing deployment with no need to rearchitect or rewrite code.

Istio parts
- **Mixer** enforces access control and usage policies across the service mesh, and collects telemetry data from the Envoy proxy and other services
- **Pilot** provides service discovery for the Envoy sidecars, traffic management capabilities for intelligent routing 
- **Citadel** enables strong service-to-service and end-user authentication with built-in identity and credential management. 
- **Galley** is Istio’s configuration validation, ingestion, processing and distribution component. 

