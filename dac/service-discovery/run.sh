kubectl delete --all pods && \
kubectl delete --all deployments && \
kubectl delete --all services

docker build --rm -t avincze73/k8s-hello-world-db:v1 .
docker push avincze73/k8s-hello-world-db:v1

kubectl apply -f secret.yml
kubectl apply -f database.yml
kubectl apply -f database-service.yml
kubectl apply -f helloworld-db.yml
kubectl apply -f helloworld-db-service.yml
kubectl logs helloworld-deployment-db-757b8fbd69-gg5dv
minikube service helloworld-db-service --url

kubectl run -it --tty busybox --image=busybox --restart=Never -- sh









