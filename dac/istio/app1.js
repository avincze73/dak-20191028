'use strict';

const express = require('express')
const req = require('request');
const app = express()
const port = 8080
var text = process.env.TEXT;
var next = process.env.NEXT;

const options = {
  hostname: 'index.hu',
  protocol: 'https',
  uri: '/',
  method: 'GET',
  timeout: 2000
}


app.get('/', (request, response) => {
  req('http://' + next, function (error, resp, body) {
    if (!error && resp.statusCode == 200) {
      response.send(text + ' ' + body); // Print the google web page.
    }
  });

})

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})
