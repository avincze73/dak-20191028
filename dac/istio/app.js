'use strict';

const express = require('express')
const req = require('request');
const app = express()
const port = 8080
var text = process.env.TEXT;
var next = process.env.NEXT;

const options = {
  hostname: 'index.hu',
  protocol: 'https',
  uri: '/',
  method: 'GET',
  timeout: 2000
}


app.get('/', (request, response) => {
  response.send("hello from app service");
})

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})
