#Istio

## Creating a cluster for istio

```bash
aws configure
```
```bash
kops create cluster --name=provinlog.ml --state=s3://kops-state-sa1973 \
--zones=eu-west-1a --node-count=2 --node-size=t2.medium \
--master-size=t2.micro --dns-zone=provinlog.ml
```

Adding new cluster features
```bash
kops edit cluster provinlog.ml --state=s3://kops-state-sa1973
```
After the spec: section add the following features
```bash
  kubeAPIServer:
    admissionControl:
    - NamespaceLifecycle
    - LimitRanger
    - ServiceAccount
    - PersistentVolumeLabel
    - DefaultStorageClass
    - DefaultTolerationSeconds
    - MutatingAdmissionWebhook
    - ValidatingAdmissionWebhook
    - ResourceQuota
    - NodeRestriction
    - Priority
```

Updating the cluster
```bash
kops update cluster provinlog.ml --state=s3://kops-state-sa1973 --yes
kops validate cluster --state=s3://kops-state-sa1973 
```

## Download Istio
```
cd Downloads
curl -L https://git.io/getLatestIstio | ISTIO_VERSION=1.3.3 sh -
cd istio-1.3.3
export PATH=$PWD/bin:$PATH
for i in install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done
kubectl get pod -n istio-system
kubectl get svc -n istio-system
```

## Demo system
![Microservice](../image/03-istio-demo.png)



## Creating the applications

```bash
docker build  --rm -t avincze73/app1:v1 -f Dockerfile.app1  .
docker push avincze73/app1:v1
```

```bash
kubectl apply -f apps.yaml
```

## Installing the service mesh
```bash
kubectl apply -f <(istioctl kube-inject -f istio/apps.yaml)
kubectl apply -f istio/apps-gw.yaml
kubectl get svc -n istio-system
```


Because the httpbin service is not exposed outside of the cluster
we cannot _curl_ it directly, however we can verify that it is working correctly using
a _curl_ command against `httpbin:8000` *from inside the cluster* using the public _dockerqa/curl_
image from the Docker hub:

```bash
kubectl run -i --rm --restart=Never dummy --image=dockerqa/curl:ubuntu-trusty --command -- curl --silent app:8080/html
```

## Istio routing
![Microservice](../image/03-istio-routing.png)

## Canary routing
![Microservice](../image/03-istio-canary.png)
It is a good way to test a new version of the application only on the given percentage of the population.


```bash
for ((i=1;i<11;i++)); do curl a6cf80178f3f111e9bc7d0ab29a8ee7d-630238595.eu-west-1.elb.amazonaws.com/ -H "Host: sprint.com"; done
```

## Retries
One of a great feature of istio is retries. Pod latency means it has to wait a given amount of the before executing the code.
![Microservice](../image/03-istio-retry.png)