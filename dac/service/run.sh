kubectl delete --all pods && \
kubectl delete --all deployments && \
kubectl delete --all services
kubectl create -f helloworld-pod.yaml
kubectl create -f helloworld-nodeport-service.yaml
minikube service helloworld-service --url
kubectl describe svc helloworld-service