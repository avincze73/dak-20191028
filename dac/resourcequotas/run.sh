kubectl delete --all pods && \
kubectl delete --all deployments && \
kubectl delete --all services


kubectl create -f resourcequota.yml
kubectl create -f helloworld-no-quotas.yml
kubectl get deploy --namespace=sprint-ns
kubectl get rs --namespace=sprint-ns
kubectl describe rs helloworld-deployment-56d584dc89 --namespace=sprint-ns
kubectl delete deployment helloworld-deployment --namespace=sprint-ns
kubectl create -f helloworld-with-quotas.yml
kubectl describe pod helloworld-deployment-b46d94b7d-2wwpj -n sprint-ns
kubectl describe quota -n sprint-ns

kubectl create -f defaults.yml
kubectl describe limits limits -n sprint-ns


