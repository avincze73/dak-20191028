kubectl delete --all pods && \
kubectl delete --all deployments && \
kubectl delete --all services


kubectl apply -f wordpress-secrets.yml
kubectl apply -f wordpress-single-deployment-no-volumes.yml
kubectl apply -f wordpress-service.yml
minikube service wordpress-service --url
