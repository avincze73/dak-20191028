kubectl delete --all pods && \
kubectl delete --all deployments && \
kubectl delete --all services


minikube ssh
openssl genrsa -out attila.pem 2048
openssl req -new -key attila.pem -out attila-csr.pem -subj "/CN=attila/O=sprint/"
sudo openssl x509 -req -in attila-csr.pem -CA /var/lib/minikube/certs/ca.crt -CAkey /var/lib/minikube/certs/ca.key -CAcreateserial -out attila.crt -days 10000
cat attila.crt
cat attila.pem
vim /Users/avincze/.minikube/attila.key
vim /Users/avincze/.minikube/attila.crt
kubectl config view

kubectl config set-credentials attila --client-certificate=attila.crt --client-key=attila.pem
kubectl config set-credentials attila --client-certificate=attila.crt --client-key=attila.pem

kubectl config set-context attila --cluster=minikube --user attila
kubectl config view
kubectl get contexts


openssl genrsa -out attila.pem 2048
